//
//  AppDelegate.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.rootViewController = UINavigationController(rootViewController: SearchConfig.createModule(factory: AppInjector(),
                                                                                                          dataSource: SearchModel.DataSource()))
        return true
    }
}



// test

