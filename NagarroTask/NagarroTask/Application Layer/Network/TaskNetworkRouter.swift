//
//  TaskNetworkRouter.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
import Moya


enum TaskNetworkRouter {
    case requestMedia(request: RequestMedia)
}

extension TaskNetworkRouter: TargetType {
    var headers: [String : String]? {
        return nil
    }
    
    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }
    
    var baseURL: URL { return URL(string: "https://itunes.apple.com/")! }
    
    var path: String {
        switch self {
        case .requestMedia:
            return "search"
        }
    }
    var method: Moya.Method {
        switch self {
        default:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .requestMedia(let request):
            return .requestCompositeParameters(bodyParameters: ["term" : request.term , "entity" : request.entity],
                                               bodyEncoding: JSONEncoding.default ,
                                               urlParameters: ["term" : request.term , "entity" : request.entity])
        }
    }
}
