//
//  TaskService.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
import Moya

protocol TaskServiceProtocol {
    func getMediaTypes(completion: @escaping (Result<[MediaTypesResponse], Error>) -> Void)
    func getSearchResults(searchKeyword: String, mediaTypes: String, completion: @escaping (Result<[SearchResult], Error>) -> Void)
}

final class TaskService: TaskServiceProtocol {

    private var networkRouter: MoyaProvider<TaskNetworkRouter>
    private var jsonTransformer: JsonTransforming
    
    init(jsonTransformer: JsonTransforming, networkRouter: MoyaProvider<TaskNetworkRouter>) {
        self.networkRouter =   networkRouter
        self.jsonTransformer = jsonTransformer
    }
    
    func getMediaTypes(completion: @escaping (Result<[MediaTypesResponse], Error>) -> Void) {
        var mediaTypes = [MediaTypesResponse]()
        mediaTypes.append(.init(id: "musicVideo", title: "Music Video"))
        mediaTypes.append(.init(id: "podcast", title: "Podcast"))
        completion(.success(mediaTypes))
    }
    
    func getSearchResults(searchKeyword: String, mediaTypes: String, completion: @escaping (Result<[SearchResult], Error>) -> Void) {
        networkRouter.request(.requestMedia(request: .init(term: searchKeyword, entity: mediaTypes))) { [weak self] (result) in
            guard let self = self else {return}
            switch result{
            case.success(let response):
                print(response.description)
                if response.statusCode == 200{
                    guard let responseModel = self.jsonTransformer.decodeObject(from: response.data, to: SearchResultsResponse.self, with: nil) else{ return }
                    completion(.success(responseModel.results ?? [SearchResult]()))
                }
            case.failure(let error):
                completion(.failure(error))
            }
        }
    }
}
