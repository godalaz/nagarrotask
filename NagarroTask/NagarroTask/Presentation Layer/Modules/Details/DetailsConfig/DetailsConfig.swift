//
//  DetailsConfig.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

struct DetailsConfig {
    
    typealias DetailsFactory = DetailsInteractorFactorable & DetailsRouterFactorable
    
    static func createModule(factory: DetailsFactory, dataSource: DetailsModel.DataSource) -> DetailsViewController {
        let view = UIStoryboard(name: R.storyboard.main.name, bundle: nil).instantiateViewController(withIdentifier: R.storyboard.main.detailsViewController.identifier) as! DetailsViewController
        let interactorFactory = factory as! DetailsInteractorFactorable.InteractableFactory
        view.interactor = factory.makeInteractor(factory: interactorFactory, viewController: view, dataSource: dataSource)
        view.router = factory.makeRouter(viewController: view)
        return view
    }
}
