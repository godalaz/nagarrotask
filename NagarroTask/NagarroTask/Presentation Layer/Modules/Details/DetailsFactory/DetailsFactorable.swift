//
//  DetailsFactorable.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
import UIKit

extension AppInjector: DetailsFactorable{
  struct DetailsInjector {}
}

extension AppInjector.DetailsInjector: DetailsFactorable {}

protocol DetailsFactorable: DetailsInteractorFactorable, DetailsPresenterFactorable, DetailsRouterFactorable, DetailsServicesFactorable { }

protocol DetailsInteractorFactorable {
  typealias InteractableFactory = DetailsPresenterFactorable & DetailsServicesFactorable
  
  func makeInteractor(factory: InteractableFactory, viewController: DetailsDisplayLogic?, dataSource: DetailsModel.DataSource) -> DetailsInteractable
}

protocol DetailsPresenterFactorable {
  func makePresenter(viewController: DetailsDisplayLogic?) -> DetailsPresentationLogic
}

protocol DetailsRouterFactorable {
  func makeRouter(viewController: UIViewController?) -> DetailsRouting
}

extension DetailsFactorable {
  
  func makeInteractor(factory: InteractableFactory, viewController: DetailsDisplayLogic?, dataSource: DetailsModel.DataSource) -> DetailsInteractable {
    DetailsInteractor(factory: factory, viewController: viewController, dataSource: dataSource)
  }
  
  func makePresenter(viewController: DetailsDisplayLogic?) -> DetailsPresentationLogic {
    DetailsPresenter(viewController: viewController)
  }
  
  func makeRouter(viewController: UIViewController?) -> DetailsRouting {
    DetailsRouter(viewController: viewController)
  }
}


protocol DetailsServicesFactorable {
}
