//
//  DetailsInteractor.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import Foundation
import Foundation
typealias DetailsInteractable = DetailsBusinessLogic & DetailsDataStore

protocol DetailsBusinessLogic {
    func doRequest(_ request: DetailsModel.Request)
    func onScreenAppeard()
}

protocol DetailsDataStore {
    var dataSource: DetailsModel.DataSource { get }
}

final class DetailsInteractor: DetailsDataStore {
    
    var dataSource: DetailsModel.DataSource
    
    private var factory: DetailsInteractorFactorable.InteractableFactory
    private var presenter: DetailsPresentationLogic
    
    init(factory: DetailsInteractorFactorable.InteractableFactory, viewController: DetailsDisplayLogic?, dataSource: DetailsModel.DataSource) {
        self.factory = factory
        self.dataSource = dataSource
        self.presenter = factory.makePresenter(viewController: viewController)
    }
}


// MARK: - DetailsBusinessLogic
extension DetailsInteractor: DetailsBusinessLogic {
    func onScreenAppeard() {
        print("projecct setup successfully..")
    }
    func doRequest(_ request: DetailsModel.Request) {}
}
