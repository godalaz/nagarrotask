//
//  MediaTypesResponse.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
struct MediaTypesResponse: Codable, Equatable{
    let id: String
    let title: String
}
