//
//  MediaTypesInteractor.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation

typealias MediaTypesInteractable = MediaTypesBusinessLogic & MediaTypesDataStore

protocol MediaTypesBusinessLogic {
    func doRequest(_ request: MediaTypesModel.Request)
}

protocol MediaTypesDataStore {
    var dataSource: MediaTypesModel.DataSource { get }
}

final class MediaTypesInteractor: MediaTypesDataStore {
    
    var dataSource: MediaTypesModel.DataSource
    
    private var factory: MediaTypesInteractorFactorable.InteractableFactory
    private var presenter: MediaTypesPresentationLogic
    private var service: TaskServiceProtocol
    
    init(factory: MediaTypesInteractorFactorable.InteractableFactory, viewController: MediaTypesDisplayLogic?, dataSource: MediaTypesModel.DataSource) {
        self.factory = factory
        self.dataSource = dataSource
        self.presenter = factory.makePresenter(viewController: viewController)
        self.service = factory.makeTaskService()
    }
}


// MARK: - MediaTypesBusinessLogic
extension MediaTypesInteractor: MediaTypesBusinessLogic {
    func doRequest(_ request: MediaTypesModel.Request) {
        switch request {
        case .getMediaTypes:
            self.getMediaTypes()
        }
    }
}


private extension MediaTypesInteractor {
    func getMediaTypes(){
        service.getMediaTypes { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let mediaTypes):
                self.presenter.presentResponse(.mediaTypes(response: mediaTypes))
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
