//
//  MediaTypesConfig.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

struct MediaTypesConfig {
    
    typealias MediaTypesFactory = MediaTypesInteractorFactorable & MediaTypesRouterFactorable
    
    static func createModule(factory: MediaTypesFactory, dataSource: MediaTypesModel.DataSource) -> MediaTypesViewController {
        let view = UIStoryboard(name: R.storyboard.main.name, bundle: nil).instantiateViewController(withIdentifier: R.storyboard.main.mediaTypesViewController.identifier) as! MediaTypesViewController
        let interactorFactory = factory as! MediaTypesInteractorFactorable.InteractableFactory
        view.interactor = factory.makeInteractor(factory: interactorFactory, viewController: view, dataSource: dataSource)
        view.router = factory.makeRouter(viewController: view)
        return view
    }
}
