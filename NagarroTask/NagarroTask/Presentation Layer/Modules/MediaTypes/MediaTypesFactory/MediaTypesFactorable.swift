//
//  MediaTypesFactorable.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit
import Moya

extension AppInjector: MediaTypesFactorable{
  struct MediaTypesInjector {}
}

extension AppInjector.MediaTypesInjector: MediaTypesFactorable {}

protocol MediaTypesFactorable: MediaTypesInteractorFactorable, MediaTypesPresenterFactorable, MediaTypesRouterFactorable, TaskServicesFactorable { }

protocol MediaTypesInteractorFactorable {
  typealias InteractableFactory = MediaTypesPresenterFactorable & TaskServicesFactorable
  
  func makeInteractor(factory: InteractableFactory, viewController: MediaTypesDisplayLogic?, dataSource: MediaTypesModel.DataSource) -> MediaTypesInteractable
}

protocol MediaTypesPresenterFactorable {
  func makePresenter(viewController: MediaTypesDisplayLogic?) -> MediaTypesPresentationLogic
}

protocol MediaTypesRouterFactorable {
  func makeRouter(viewController: UIViewController?) -> MediaTypesRouting
}

extension MediaTypesFactorable {
  
  func makeInteractor(factory: InteractableFactory, viewController: MediaTypesDisplayLogic?, dataSource: MediaTypesModel.DataSource) -> MediaTypesInteractable {
    MediaTypesInteractor(factory: factory, viewController: viewController, dataSource: dataSource)
  }
  
  func makePresenter(viewController: MediaTypesDisplayLogic?) -> MediaTypesPresentationLogic {
    MediaTypesPresenter(viewController: viewController)
  }
  
  func makeRouter(viewController: UIViewController?) -> MediaTypesRouting {
    MediaTypesRouter(viewController: viewController)
  }
}


protocol TaskServicesFactorable {
    func makeTaskService() -> TaskServiceProtocol
}

extension TaskServicesFactorable{
    func makeTaskService() -> TaskServiceProtocol{
         let plugin: PluginType = NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))
         let provider = MoyaProvider<TaskNetworkRouter>(plugins: [plugin])
        return TaskService(jsonTransformer: CodableTransformer(), networkRouter: provider)
    }
}
