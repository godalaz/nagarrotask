//
//  MediaTypesPresenter.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
protocol MediaTypesPresentationLogic {
    func presentResponse(_ response: MediaTypesModel.Response)
}

final class MediaTypesPresenter {
    
    private weak var viewController: MediaTypesDisplayLogic?
    private let transfromer: MediaTypesTransformerProtocol
    
    init(viewController: MediaTypesDisplayLogic?) {
        self.viewController = viewController
        self.transfromer = MediaTypesTransformer()
    }
}


// MARK: - MediaTypesPresentationLogic
extension MediaTypesPresenter: MediaTypesPresentationLogic {
    
    func presentResponse(_ response: MediaTypesModel.Response) {
        switch response {
        case .mediaTypes(response: let mediaTypes):
            self.presentMediaTypes(with: mediaTypes)
        }
    }
}

private extension MediaTypesPresenter {
    func presentMediaTypes(with mediaTypes: [MediaTypesResponse]){
        viewController?.displayViewModel(.mediaTypes(viewModel: self.transfromer.transformMediaTypesToViewModel(model: mediaTypes)))
        viewController?.hideLoading()
    }
}
