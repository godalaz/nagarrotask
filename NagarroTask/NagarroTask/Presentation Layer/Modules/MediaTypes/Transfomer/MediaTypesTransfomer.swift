//
//  MediaTypesTransfomer.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation

protocol MediaTypesTransformerProtocol {
    func transformMediaTypesToViewModel(model: [MediaTypesResponse]) -> [MediaTypesViewModel]
}

struct MediaTypesTransformer: MediaTypesTransformerProtocol{
    
    func transformMediaTypesToViewModel(model: [MediaTypesResponse]) -> [MediaTypesViewModel]{
        var mediaTypesViewModel = [MediaTypesViewModel]()
        model.forEach{
            mediaTypesViewModel.append(.init(id: $0.id, title: $0.title))
        }
        return mediaTypesViewModel
    }
}
