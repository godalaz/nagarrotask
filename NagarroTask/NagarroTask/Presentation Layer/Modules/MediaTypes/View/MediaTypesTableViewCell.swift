//
//  MediaTypesTableViewCell.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

class MediaTypesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mediaTitle: UILabel!
    
    func configureCell(viewModel: MediaTypesViewModel){
        mediaTitle.text = viewModel.title
    }
}
