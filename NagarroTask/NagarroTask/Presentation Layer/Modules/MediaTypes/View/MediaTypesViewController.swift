//
//  MediaTypesViewController.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

protocol MediaTypesDisplayLogic: class, BaseViewControllerProtocol{
    func displayViewModel(_ viewModel: MediaTypesModel.ViewModel)
}

final class MediaTypesViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var interactor: MediaTypesInteractable!
    var router: MediaTypesRouting!
    weak var delegate: GetSelectedMediaTypesProtocol?
    
    private var viewModel = [MediaTypesViewModel]()
    private var selectedTypes = [MediaTypesViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup(){
        title = "Select media types"
        setupTableView()
        addDoneButtonToNavigationBar()
        showLoading()
        interactor.doRequest(.getMediaTypes)
    }
    
    private func addDoneButtonToNavigationBar(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(addTapped))
    }
    
    @objc func addTapped(sender: UIBarButtonItem) {
        self.delegate?.selectedMediaTypes(viewModel: selectedTypes)
        router.routeTo(.backToSearchScreen)
    }
    
    private func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        tableView.allowsMultipleSelection = true
        tableView.rowHeight = 75.0
    }
}

extension MediaTypesViewController: MediaTypesDisplayLogic {
    func displayViewModel(_ viewModel: MediaTypesModel.ViewModel) {
        switch viewModel {
        case .mediaTypes(viewModel: let viewModel):
            self.viewModel = viewModel
            self.tableView.reloadData()
        }
    }
}

extension MediaTypesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediaTypesTableViewCell", for: indexPath) as! MediaTypesTableViewCell
        cell.configureCell(viewModel: self.viewModel[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
        
        if let index = selectedTypes.firstIndex(of: viewModel[indexPath.row]) {
            selectedTypes.remove(at: index)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
        }
        selectedTypes.append(viewModel[indexPath.row])
    }
}
