//
//  SearchModel.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//



import Foundation

enum SearchModel {
    
    enum Request {
        case searchResult(searchKeyword: String?, selectedMediaTypes: [String]?)
    }
    
    enum Response {
        case successFillSearchData
    }
    
    enum ViewModel {
        case navigateToSearchData
    }
    
    enum Route {
        case dismiss
        case MediaTypes
        case searchResult(searchKeyword: String, selectedMediaTypes: [String])
    }
    
    struct DataSource {
        var searchKeyword: String?
        var selectedMediaTypes: [String]?
    }
    
    struct SearchError {
        let errorTitle: String?
    }
}
