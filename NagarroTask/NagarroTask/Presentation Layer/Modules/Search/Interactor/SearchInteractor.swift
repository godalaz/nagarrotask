//
//  SearchInteractor.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import Foundation

typealias SearchInteractable = SearchBusinessLogic & SearchDataStore

protocol SearchBusinessLogic {
    func doRequest(_ request: SearchModel.Request)
}

protocol SearchDataStore {
    var dataSource: SearchModel.DataSource { get }
}

final class SearchInteractor: SearchDataStore {
    
    var dataSource: SearchModel.DataSource
    
    private var factory: SearchInteractorFactorable.InteractableFactory
    private var presenter: SearchPresentationLogic
    
    init(factory: SearchInteractorFactorable.InteractableFactory, viewController: SearchDisplayLogic?, dataSource: SearchModel.DataSource) {
        self.factory = factory
        self.dataSource = dataSource
        self.presenter = factory.makePresenter(viewController: viewController)
    }
}


// MARK: - SearchBusinessLogic
extension SearchInteractor: SearchBusinessLogic {
    func doRequest(_ request: SearchModel.Request) {
        switch request {
        case .searchResult(searchKeyword: let searchKeyword, selectedMediaTypes: let selectedMediaTypes):
            self.prepareDataToGetSearchResults(searchKeyword: searchKeyword, selectedMediaTypes: selectedMediaTypes)
        }
    }
}

private extension SearchInteractor{
    func prepareDataToGetSearchResults(searchKeyword: String?, selectedMediaTypes: [String]?){
        guard let searchKey = searchKeyword, searchKey.count > 0 else {
            presenter.presentError(error: .init(errorTitle: "search keywork is required"))
            return
        }
        
        guard let selectedMediaTypes = selectedMediaTypes, selectedMediaTypes.count > 0 else {
            presenter.presentError(error: .init(errorTitle: "choose at least 1 media type"))
            return
        }
        
        presenter.presentResponse(.successFillSearchData)
    }
}
