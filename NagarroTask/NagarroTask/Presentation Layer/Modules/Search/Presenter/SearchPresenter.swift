//
//  SearchPresenter.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import Foundation

protocol SearchPresentationLogic {
    func presentResponse(_ response: SearchModel.Response)
    func presentError(error: SearchModel.SearchError)
}

final class SearchPresenter {
    
    private weak var viewController: SearchDisplayLogic?
    
    init(viewController: SearchDisplayLogic?) {
        self.viewController = viewController
    }
    
}


// MARK: - SearchPresentationLogic
extension SearchPresenter: SearchPresentationLogic {
    
    func presentError(error: SearchModel.SearchError) {
        viewController?.displayError(error)
    }
    
    func presentResponse(_ response: SearchModel.Response) {
        switch response {
        case .successFillSearchData:
            viewController?.displayViewModel(.navigateToSearchData)
        }
    }
}

