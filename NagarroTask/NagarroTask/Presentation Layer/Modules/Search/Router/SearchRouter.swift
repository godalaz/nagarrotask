//
//  SearchRouter.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import UIKit

protocol SearchRouting {
    
    func routeTo(_ route: SearchModel.Route)
}

final class SearchRouter {
    
    private weak var viewController: UIViewController?
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
    }
}


// MARK: - SearchRouting
extension SearchRouter: SearchRouting {
    
    func routeTo(_ route: SearchModel.Route) {
        DispatchQueue.main.async {
            switch route {
            case .MediaTypes:
                let mediaTypesViewController = MediaTypesConfig.createModule(factory: AppInjector(), dataSource: MediaTypesModel.DataSource())
                mediaTypesViewController.delegate = self.viewController as? GetSelectedMediaTypesProtocol
                self.viewController?.navigationController?.pushViewController(mediaTypesViewController , animated: true)
                
            case .searchResult(dataSource: let dataSource):
                self.viewController?.navigationController?.pushViewController(SearchResultConfig.createModule(factory: AppInjector(),
                                                                                                              dataSource: .init(searchKeyword: dataSource.searchKeyword ,
                                                                                                                                selectedMedia: dataSource.selectedMediaTypes)),animated: true)
            case .dismiss:
                self.viewController?.dismiss(animated: true, completion: nil)
            }
        }
    }
}
