//
//  SearchConfig.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit
struct SearchConfig {
    
    typealias SearchFactory = SearchInteractorFactorable & SearchRouterFactorable
    
    static func createModule(factory: SearchFactory, dataSource: SearchModel.DataSource) -> SearchViewController {
        let view = UIStoryboard(name: R.storyboard.main.name, bundle: nil).instantiateViewController(withIdentifier: R.storyboard.main.searchViewController.identifier) as! SearchViewController
        let interactorFactory = factory as! SearchInteractorFactorable.InteractableFactory
        view.interactor = factory.makeInteractor(factory: interactorFactory, viewController: view, dataSource: dataSource)
        view.router = factory.makeRouter(viewController: view)
        return view
    }
}
