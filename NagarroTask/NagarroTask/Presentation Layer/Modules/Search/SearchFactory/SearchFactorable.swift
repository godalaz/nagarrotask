//
//  SearchFactorable.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import UIKit


extension AppInjector: SearchFactorable{
  struct SearchInjector {}
}

extension AppInjector.SearchInjector: SearchFactorable {}

protocol SearchFactorable: SearchInteractorFactorable, SearchPresenterFactorable, SearchRouterFactorable, SearchServicesFactorable { }

protocol SearchInteractorFactorable {
  typealias InteractableFactory = SearchPresenterFactorable & SearchServicesFactorable
  
  func makeInteractor(factory: InteractableFactory, viewController: SearchDisplayLogic?, dataSource: SearchModel.DataSource) -> SearchInteractable
}

protocol SearchPresenterFactorable {
  func makePresenter(viewController: SearchDisplayLogic?) -> SearchPresentationLogic
}

protocol SearchRouterFactorable {
  func makeRouter(viewController: UIViewController?) -> SearchRouting
}

extension SearchFactorable {
  
  func makeInteractor(factory: InteractableFactory, viewController: SearchDisplayLogic?, dataSource: SearchModel.DataSource) -> SearchInteractable {
    SearchInteractor(factory: factory, viewController: viewController, dataSource: dataSource)
  }
  
  func makePresenter(viewController: SearchDisplayLogic?) -> SearchPresentationLogic {
    SearchPresenter(viewController: viewController)
  }
  
  func makeRouter(viewController: UIViewController?) -> SearchRouting {
    SearchRouter(viewController: viewController)
  }
}


protocol SearchServicesFactorable {
}
