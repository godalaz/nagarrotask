//
//  SearchViewController.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import UIKit

protocol SearchDisplayLogic: class, BaseViewControllerProtocol{
    func displayViewModel(_ viewModel: SearchModel.ViewModel)
    func displayError(_ error: SearchModel.SearchError)
}

protocol GetSelectedMediaTypesProtocol: class {
    func selectedMediaTypes(viewModel: [MediaTypesViewModel])
}

final class SearchViewController: BaseViewController {
    
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chooseMedia: UIButton!
    
    var interactor: SearchInteractable!
    var router: SearchRouting!
    private var selectedMediaTypes = [MediaTypesViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup(){
        setupUI()
    }
    
    private func setupUI(){
        addBorder(button: [chooseMedia, submitButton])
        setupTableview()
    }
    
    private func addBorder(button: [UIButton]){
        button.forEach{
            $0.layer.cornerRadius = 12
        }
    }
    
    private func setupTableview(){
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.register(TagListTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self

    }
    @IBAction func chooseMediaButtonPressed(_ sender: Any) {
        router.routeTo(.MediaTypes)
    }
    
    @IBAction func SearchResultButtonPressed(_ sender: Any) {
        interactor.doRequest(.searchResult(searchKeyword: self.searchTF.text, selectedMediaTypes: selectedMediaTypes.map{return $0.title}))
    }
}

extension SearchViewController: SearchDisplayLogic {
    
    func displayError(_ error: SearchModel.SearchError) {
        self.showError(with: error.errorTitle ?? "")
    }
    
    func displayViewModel(_ viewModel: SearchModel.ViewModel) {
        switch viewModel {
        case .navigateToSearchData:
            router.routeTo(.searchResult(searchKeyword: self.searchTF.text!, selectedMediaTypes: self.selectedMediaTypes.map{return $0.id}))
        }
    }
}

extension SearchViewController: GetSelectedMediaTypesProtocol{
    func selectedMediaTypes(viewModel: [MediaTypesViewModel]) {
        selectedMediaTypes = viewModel
        tableView.reloadData()
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TagListTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configureCell(with: selectedMediaTypes)
        return cell
    }
}
