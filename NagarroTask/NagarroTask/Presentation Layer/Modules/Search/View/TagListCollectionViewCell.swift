//
//  TagListCollectionViewCell.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//

import UIKit

class TagListCollectionViewCell: UICollectionViewCell, NibBased{
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.parentView.layer.cornerRadius = 12
    }
    
    func configureCell(title: String){
        self.titleLabel.text = title
    }
    
}
