//
//  TagListTableViewCell.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//

import UIKit

class TagListTableViewCell: UITableViewCell, NibBased{

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var model = [MediaTypesViewModel]()

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(TagListCollectionViewCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = false
        collectionView.collectionViewLayout = TagListFlowLayout()
        collectionView.invalidateIntrinsicContentSize()
        updateRowHeight()
    }

    func configureCell(with model: [MediaTypesViewModel]){
        self.model = model
        self.collectionView.reloadData()
    }
    
    private func updateRowHeight() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            self.tableView?.updateRowHeightsWithoutReloadingRows()
        }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return collectionView.contentSize
    }
}
extension TagListTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TagListCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.configureCell(title: model[indexPath.row].title)
        return cell
    }
}

extension TagListTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var tag = ""
        if model.count > 0 {
            tag = self.model[indexPath.row].title
        }
        let font =  UIFont(name: "Avenir", size: 17)!
        let size = tag.size(withAttributes: [NSAttributedString.Key.font: font])
        var dynamicCellWidth: Double = 0.0
        if model.count > 0 {
            dynamicCellWidth = Double(size.width)
        }else{
            dynamicCellWidth = Double(size.width + 24)
        }
        return CGSize(width: dynamicCellWidth + 20, height: 31)
    }
    
    // Space between rows
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    // Space between cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

class TagListFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
         let attributesForElementsInRect = super.layoutAttributesForElements(in: rect)
         var newAttributesForElementsInRect = [UICollectionViewLayoutAttributes]()
         
         var leftMargin: CGFloat = 0.0;
         
         for attributes in attributesForElementsInRect! {
             if (attributes.frame.origin.x == self.sectionInset.left) {
                 leftMargin = self.sectionInset.left
             } else {
                 var newLeftAlignedFrame = attributes.frame
                 newLeftAlignedFrame.origin.x = leftMargin
                 attributes.frame = newLeftAlignedFrame
             }
             leftMargin += attributes.frame.size.width + 8 // Makes the space between cells
             newAttributesForElementsInRect.append(attributes)
         }
         return newAttributesForElementsInRect
     }
}
