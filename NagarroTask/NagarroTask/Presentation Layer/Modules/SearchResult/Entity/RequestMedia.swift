//
//  RequestMedia.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
struct RequestMedia: Codable {
    let term: String
    let entity: String
}
