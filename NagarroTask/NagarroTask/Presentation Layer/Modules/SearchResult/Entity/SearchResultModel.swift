//
//  SearchResultModel.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation

enum SearchResultModel {
    
    enum Request {
        case getSearchResults
    }
    
    enum Response {
        case getSearchResults(response: [SearchResult])
    }
    
    enum ViewModel {
        case searchResultsViewModel(viewModel: [SearchResultsViewModel])
    }
    
    enum Route {
        case Details
    }
    
    struct DataSource {
        let searchKeyword: String
        let selectedMedia: [String]
    }
}
