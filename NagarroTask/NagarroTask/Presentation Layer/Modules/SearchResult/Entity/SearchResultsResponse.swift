//
//  SearchResultsResponse.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
struct SearchResultsResponse: Codable{
    let resultCount: Int?
    let results: [SearchResult]?
}

// MARK: - Result
struct SearchResult: Codable,Equatable{
    let wrapperType: String?
    let kind: String?
    let artistid: Int?
    let collectionid: Int?
    let trackid: Int?
    let artistName: String?
    let collectionName: String?
    let trackName: String?
    let collectionCensoredName: String?
    let trackCensoredName: String?
    let artistViewurl: String?
    let collectionViewurl: String?
    let trackViewurl: String?
    let previewurl: String?
    let artworkUrl30: String?
    let artworkUrl60: String?
    let artworkUrl100: String?
    let collectionPrice: Double?
    let trackPrice: Double?
    let releaseDate: String?
    let collectionExplicitness: String?
    let trackExplicitness: String?
    let discCount: Int?
    let discNumber: Int?
    let trackCount: Int?
    let trackNumber: Int?
    let trackTimeMillis: Int?
    let country: String?
    let currency: String?
    let primaryGenreName: String?
    let contentAdvisoryRating: String?
}
