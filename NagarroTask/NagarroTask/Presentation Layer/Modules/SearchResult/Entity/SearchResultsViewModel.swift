//
//  SearchResultsViewModel.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
struct SearchResultsViewModel: Codable, Equatable{
    let image: String
    let title: String
    let trackName: String
}
