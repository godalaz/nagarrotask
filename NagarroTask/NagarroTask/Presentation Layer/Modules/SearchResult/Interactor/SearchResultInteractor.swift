//
//  SearchResultInteractor.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
typealias SearchResultInteractable = SearchResultBusinessLogic & SearchResultDataStore

protocol SearchResultBusinessLogic {
    func doRequest(_ request: SearchResultModel.Request)
}

protocol SearchResultDataStore {
    var dataSource: SearchResultModel.DataSource { get }
}

final class SearchResultInteractor: SearchResultDataStore {
    
    var dataSource: SearchResultModel.DataSource
    
    private var factory: SearchResultInteractorFactorable.InteractableFactory
    private var presenter: SearchResultPresentationLogic
    private var service: TaskServiceProtocol
    private var searchResult: [SearchResult]
    private let dispatchGroup: DispatchGroup
    
    init(factory: SearchResultInteractorFactorable.InteractableFactory, viewController: SearchResultDisplayLogic?, dataSource: SearchResultModel.DataSource) {
        self.factory = factory
        self.dataSource = dataSource
        self.presenter = factory.makePresenter(viewController: viewController)
        self.service = factory.makeTaskService()
        self.searchResult = []
        self.dispatchGroup = DispatchGroup()
    }
}


// MARK: - SearchResultBusinessLogic
extension SearchResultInteractor: SearchResultBusinessLogic {

    func doRequest(_ request: SearchResultModel.Request) {
        switch request {
        case .getSearchResults:
            self.getSearcchResults()
        }
    }
}

private extension SearchResultInteractor {
    func getSearcchResults(){
        dataSource.selectedMedia.forEach{
            dispatchGroup.enter()
            
            service.getSearchResults(searchKeyword: dataSource.searchKeyword, mediaTypes: $0) { [weak self] result in
                guard let self = self else {return}
                switch result {
                case .success(let searchResult):
                    self.searchResult.append(contentsOf: searchResult)
                    self.dispatchGroup.leave()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            guard let self = self else {return}
            self.presenter.presentResponse(.getSearchResults(response: self.searchResult))
        }
    }
}
