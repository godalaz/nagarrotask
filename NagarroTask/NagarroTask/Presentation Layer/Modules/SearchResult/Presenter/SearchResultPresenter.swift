//
//  SearchResultPresenter.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
protocol SearchResultPresentationLogic {
    func presentResponse(_ response: SearchResultModel.Response)
}

final class SearchResultPresenter {
    

    private weak var viewController: SearchResultDisplayLogic?
    private let transfromer: SearchResultsTransformerProtocol

    
    init(viewController: SearchResultDisplayLogic?) {
        self.viewController = viewController
        self.transfromer = SearchResultsTransformer()
    }
    
}


// MARK: - SearchResultPresentationLogic
extension SearchResultPresenter: SearchResultPresentationLogic {
    func presentResponse(_ response: SearchResultModel.Response) {
        switch response {
        case .getSearchResults(response: let response):
            presentSearchResultViewModel(results: response)
        }
    }
}

private extension SearchResultPresenter {
    func presentSearchResultViewModel(results: [SearchResult]){
        viewController?.displayViewModel(.searchResultsViewModel(viewModel: transfromer.tranformSearchResultToViewModel(results: results)))
        viewController?.hideLoading()
    }
}

