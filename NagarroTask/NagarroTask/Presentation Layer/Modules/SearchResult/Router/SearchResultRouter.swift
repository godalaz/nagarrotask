//
//  SearchResultRouter.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
import UIKit

protocol SearchResultRouting {
  
  func routeTo(_ route: SearchResultModel.Route)
}

final class SearchResultRouter {
  
  private weak var viewController: UIViewController?
  
  init(viewController: UIViewController?) {
    self.viewController = viewController
  }
}


// MARK: - SearchResultRouting
extension SearchResultRouter: SearchResultRouting {
  
  func routeTo(_ route: SearchResultModel.Route) {
    DispatchQueue.main.async {
        switch route {
        case .Details:
            self.viewController?.navigationController?.pushViewController(DetailsConfig.createModule(factory: AppInjector(), dataSource: DetailsModel.DataSource()),animated: true)
        }
    }
    }
}
