//
//  SearchResultConfig.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

struct SearchResultConfig {
    
    typealias SearchResultFactory = SearchResultInteractorFactorable & SearchResultRouterFactorable
    
    static func createModule(factory: SearchResultFactory, dataSource: SearchResultModel.DataSource) -> SearchResultViewController {
        let view = UIStoryboard(name: R.storyboard.main.name, bundle: nil).instantiateViewController(withIdentifier: R.storyboard.main.searchResultViewController.identifier) as! SearchResultViewController
        let interactorFactory = factory as! SearchResultInteractorFactorable.InteractableFactory
        view.interactor = factory.makeInteractor(factory: interactorFactory, viewController: view, dataSource: dataSource)
        view.router = factory.makeRouter(viewController: view)
        return view
    }
}
