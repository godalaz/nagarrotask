//
//  SearchResultFactorable.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
import UIKit

extension AppInjector: SearchResultFactorable{
  struct SearchResultInjector {}
}

extension AppInjector.SearchResultInjector: SearchResultFactorable {}

protocol SearchResultFactorable: SearchResultInteractorFactorable, SearchResultPresenterFactorable, SearchResultRouterFactorable, TaskServicesFactorable { }

protocol SearchResultInteractorFactorable {
  typealias InteractableFactory = SearchResultPresenterFactorable & TaskServicesFactorable
  
  func makeInteractor(factory: InteractableFactory, viewController: SearchResultDisplayLogic?, dataSource: SearchResultModel.DataSource) -> SearchResultInteractable
}

protocol SearchResultPresenterFactorable {
  func makePresenter(viewController: SearchResultDisplayLogic?) -> SearchResultPresentationLogic
}

protocol SearchResultRouterFactorable {
  func makeRouter(viewController: UIViewController?) -> SearchResultRouting
}

extension SearchResultFactorable {
  
  func makeInteractor(factory: InteractableFactory, viewController: SearchResultDisplayLogic?, dataSource: SearchResultModel.DataSource) -> SearchResultInteractable {
    SearchResultInteractor(factory: factory, viewController: viewController, dataSource: dataSource)
  }
  
  func makePresenter(viewController: SearchResultDisplayLogic?) -> SearchResultPresentationLogic {
    SearchResultPresenter(viewController: viewController)
  }
  
  func makeRouter(viewController: UIViewController?) -> SearchResultRouting {
    SearchResultRouter(viewController: viewController)
  }
}
