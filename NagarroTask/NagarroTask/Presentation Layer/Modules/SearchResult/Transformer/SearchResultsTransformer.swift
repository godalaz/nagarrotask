//
//  SearchResultsTransformer.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation

protocol SearchResultsTransformerProtocol {
    func tranformSearchResultToViewModel(results: [SearchResult]) -> [SearchResultsViewModel]
}

struct SearchResultsTransformer: SearchResultsTransformerProtocol{
    
    func tranformSearchResultToViewModel(results: [SearchResult]) -> [SearchResultsViewModel]{
        var searchResultsViewModel = [SearchResultsViewModel]()
        results.forEach {
            searchResultsViewModel.append(.init(image: $0.artworkUrl100 ?? "",
                                                title: $0.artistName ?? "",
                                                trackName: $0.trackName ?? ""))
        }
        return searchResultsViewModel
    }
}
