//
//  SearchResultCollectionViewCell.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit
import DisplaySwitcher

private let avatarListLayoutSize: CGFloat = 80.0

class SearchResultCollectionViewCell: UICollectionViewCell, NibBased {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var backgroundGradientView: UIView!
    @IBOutlet weak var nameListLabel: UILabel!
    @IBOutlet weak var nameGridLabel: UILabel!
    @IBOutlet weak var statisticLabel: UILabel!
    
    @IBOutlet weak var avatarImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var nameListLabelLeadingConstraint: NSLayoutConstraint! {
        didSet {
            initialLabelsLeadingConstraintValue = nameListLabelLeadingConstraint.constant
        }
    }
    @IBOutlet weak var statisticLabelLeadingConstraint: NSLayoutConstraint!
    
    private var avatarGridLayoutSize: CGFloat = 0.0
    private var initialLabelsLeadingConstraintValue: CGFloat = 0.0
    
    func configureCell(viewModel: SearchResultsViewModel) {
        avatarImageView.loadImage(with: viewModel.image)
        nameListLabel.text = viewModel.trackName
        nameGridLabel.text = nameListLabel.text
        statisticLabel.text = viewModel.title
    }
    
    func setupGridLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
        avatarImageViewHeightConstraint.constant = ceil(
            (cellWidth - avatarListLayoutSize) * transitionProgress + avatarListLayoutSize
        )
        avatarImageViewWidthConstraint.constant = ceil(avatarImageViewHeightConstraint.constant)
        nameListLabelLeadingConstraint.constant = -avatarImageViewWidthConstraint.constant * transitionProgress + initialLabelsLeadingConstraintValue
        statisticLabelLeadingConstraint.constant = nameListLabelLeadingConstraint.constant
        backgroundGradientView.alpha = transitionProgress <= 0.5 ? 1 - transitionProgress : transitionProgress
        nameListLabel.alpha = 1 - transitionProgress
        statisticLabel.alpha = 1 - transitionProgress
    }
    
    func setupListLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
        avatarImageViewHeightConstraint.constant = ceil(
            avatarGridLayoutSize - (avatarGridLayoutSize - avatarListLayoutSize) * transitionProgress
        )
        avatarImageViewWidthConstraint.constant = avatarImageViewHeightConstraint.constant 
        nameListLabelLeadingConstraint.constant = avatarImageViewWidthConstraint.constant * transitionProgress + (initialLabelsLeadingConstraintValue - avatarImageViewHeightConstraint.constant)
        statisticLabelLeadingConstraint.constant = nameListLabelLeadingConstraint.constant
        backgroundGradientView.alpha = transitionProgress <= 0.5 ? 1 - transitionProgress : transitionProgress
        nameListLabel.alpha = transitionProgress
        statisticLabel.alpha = transitionProgress
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DisplaySwitchLayoutAttributes {
            if attributes.transitionProgress > 0 {
                if attributes.layoutState == .grid {
                    setupGridLayoutConstraints(attributes.transitionProgress,
                                               cellWidth: attributes.nextLayoutCellFrame.width)
                    avatarGridLayoutSize = attributes.nextLayoutCellFrame.width
                } else {
                    setupListLayoutConstraints(attributes.transitionProgress,
                                               cellWidth: attributes.nextLayoutCellFrame.width)
                }
            }
        }
    }
    
}
