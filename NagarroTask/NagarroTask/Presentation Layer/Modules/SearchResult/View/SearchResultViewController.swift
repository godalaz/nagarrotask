//
//  SearchResultViewController.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/27/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit
import DisplaySwitcher

private let animationDuration: TimeInterval = 0.3

private let listLayoutStaticCellHeight: CGFloat = 80
private let gridLayoutStaticCellHeight: CGFloat = 165

protocol SearchResultDisplayLogic: class, BaseViewControllerProtocol{
    func displayViewModel(_ viewModel: SearchResultModel.ViewModel)
}

final class SearchResultViewController: BaseViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var interactor: SearchResultInteractable!
    var router: SearchResultRouting!
    private var searchResultsViewModel = [SearchResultsViewModel]()
    fileprivate var isTransitionAvailable = true
    fileprivate lazy var listLayout = DisplaySwitchLayout(
        staticCellHeight: listLayoutStaticCellHeight,
        nextLayoutStaticCellHeight: gridLayoutStaticCellHeight,
        layoutState: .list
    )
    fileprivate lazy var gridLayout = DisplaySwitchLayout(
        staticCellHeight: gridLayoutStaticCellHeight,
        nextLayoutStaticCellHeight: listLayoutStaticCellHeight,
        layoutState: .grid
    )
    fileprivate var layoutState: LayoutState = .list

    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading()
        setupCollectionView()
        interactor.doRequest(.getSearchResults)
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = listLayout
        collectionView.register(SearchResultCollectionViewCell.self)
    }
    
    private func handleLayoutApperance(){
        if !isTransitionAvailable {
            return
        }
        let transitionManager: TransitionManager
        if layoutState == .list {
            layoutState = .grid
            transitionManager = TransitionManager(
                duration: animationDuration,
                collectionView: collectionView!,
                destinationLayout: gridLayout,
                layoutState: layoutState
            )
        } else {
            layoutState = .list
            transitionManager = TransitionManager(
                duration: animationDuration,
                collectionView: collectionView!,
                destinationLayout: listLayout,
                layoutState: layoutState
            )
        }
        transitionManager.startInteractiveTransition()
    }
    
    @IBAction func gridButtonDidTap(_ sender: UIButton) {
        if layoutState  == .grid {return}
        handleLayoutApperance()
    }
    
    @IBAction func listButtonDidTap(_ sender: UIButton) {
        if layoutState  == .list {return}
        handleLayoutApperance()
    }
    
    
}

extension SearchResultViewController: SearchResultDisplayLogic {
    func displayViewModel(_ viewModel: SearchResultModel.ViewModel) {
        switch viewModel {
        case .searchResultsViewModel(viewModel: let viewModel):
            searchResultsViewModel = viewModel
            collectionView.reloadData()
        }
    }
}
extension SearchResultViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SearchResultCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        if layoutState == .grid {
            cell.setupGridLayoutConstraints(1, cellWidth: cell.frame.width)
        } else {
            cell.setupListLayoutConstraints(1, cellWidth: cell.frame.width)
        }
        cell.configureCell(viewModel: searchResultsViewModel[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchResultsViewModel.count
    }
}

extension SearchResultViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,transitionLayoutForOldLayout fromLayout: UICollectionViewLayout,newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        return customTransitionLayout
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isTransitionAvailable = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}
