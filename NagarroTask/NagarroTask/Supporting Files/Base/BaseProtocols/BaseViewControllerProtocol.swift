//
//  BaseViewControllerProtocol.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
protocol BaseViewControllerProtocol{
    func showLoading()
    func hideLoading()
    func showError(with errorMessage: String)
}
