//
//  BaseViewController.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    private var indicator = UIActivityIndicatorView(style: .gray)

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - Base Protocol implementation
extension BaseViewController: BaseViewControllerProtocol{
    
    func showLoading() {
        indicator.center = self.view.center
        self.view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func hideLoading() {
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
    
    
    func showError(with errorMessage: String) {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}
