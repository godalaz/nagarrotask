//
//  JsonTransforming.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/28/21.
//  Copyright © 2021 Mohamed Goda. All rights reserved.
//

import Foundation
protocol JsonTransforming {
    func encodeObject<T: Codable>(from model: T) -> Data?
    func decodeObject<T: Codable>(from json: Data,to model: T.Type,with strategy: JSONDecoder.KeyDecodingStrategy?) -> T?
}
struct CodableTransformer: JsonTransforming{
    
    func encodeObject<T: Codable>(from model: T) -> Data?{
        let jsonEncoder = JSONEncoder()
        let encodedModel = try? jsonEncoder.encode(model)
        return encodedModel
    }
    
    func decodeObject<T: Codable>(from json: Data,to model: T.Type,with strategy: JSONDecoder.KeyDecodingStrategy? = nil) -> T?{
        let jsonDecoder = JSONDecoder()
        if let strategy = strategy{ jsonDecoder.keyDecodingStrategy = strategy }
        let decodedModel = try? jsonDecoder.decode(model, from: json)
        if decodedModel == nil{
            print("Failed In Decoding Model : \(model)")
        }
        return decodedModel
    }
}
