//
//  MediaTypesInteractorTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class MediaTypesInteractorTests: XCTestCase {
    
    private var presenter: MediaTypesPresenterSpy!
    private var interactor: MediaTypesInteractor!
    private var service: TaskServiceSpy!
    
    override func setUp() {
        interactor = MediaTypesInteractor(factory: self, viewController: nil, dataSource: MediaTypesModel.DataSource())
    }
    
    override func tearDown() {
        presenter = nil
        interactor = nil
    }
}


// MARK: - Tests
extension MediaTypesInteractorTests {
    
    func testMediaTypesInteractor() {
        XCTAssertNil(presenter.mockMediaTypes)
        interactor.doRequest(.getMediaTypes)
        var mockModel = [MediaTypesResponse]()
        mockModel.append(.init(id: "1", title: "test 1"))
        XCTAssertEqual(mockModel, service.mockModel)
    }
}


// MARK: - MediaTypesFactorable
extension MediaTypesInteractorTests: MediaTypesFactorable {
    
    func makePresenter(viewController: MediaTypesDisplayLogic?) -> MediaTypesPresentationLogic {
        presenter = MediaTypesPresenterSpy()
        return presenter
    }
    
    func makeTaskService() -> TaskServiceProtocol {
        service = TaskServiceSpy()
        return service
    }
}


// MARK: - Spy Classes Setup
private extension MediaTypesInteractorTests {
    
    final class MediaTypesPresenterSpy: MediaTypesPresentationLogic {
        var mockMediaTypes: [MediaTypesResponse]!

        func presentResponse(_ response: MediaTypesModel.Response) {
            switch response {
            case .mediaTypes(response: let response):
                mockMediaTypes = response
            }
        }
    }
    
    
    final class TaskServiceSpy: TaskServiceProtocol {
        var mockModel = [MediaTypesResponse]()

        func getMediaTypes(completion: @escaping (Result<[MediaTypesResponse], Error>) -> Void) {
            mockModel.append(.init(id: "1", title: "test 1"))
            completion(.success(mockModel))
        }
        
        func getSearchResults(searchKeyword: String, mediaTypes: String, completion: @escaping (Result<[SearchResult], Error>) -> Void) {}
    }
}
