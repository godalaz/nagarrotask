//
//  MediaTypesPresenterTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class MediaTypesPresenterTests: XCTestCase {
    
    private var presenter: MediaTypesPresenter!
    private var viewController: MediaTypesViewControllerSpy!
    
    override func setUp() {
        viewController = MediaTypesViewControllerSpy()
        presenter = MediaTypesPresenter(viewController: viewController)
    }
    
    override func tearDown() {
        viewController = nil
        presenter = nil
    }
}


// MARK: - Tests
extension MediaTypesPresenterTests {
    
    func testMediaTypesPresenter() {
        XCTAssertNil(viewController.mockModel)
        var mediaTypesMock = [MediaTypesResponse]()
        let transfomer = MediaTypesTransformer()
    
        mediaTypesMock.append(.init(id: "1", title: "test 1"))
        mediaTypesMock.append(.init(id: "2", title: "test 2"))
        
        presenter.presentResponse(.mediaTypes(response: mediaTypesMock))
        XCTAssertEqual(viewController.mockModel, transfomer.transformMediaTypesToViewModel(model: mediaTypesMock))

    }
}



// MARK: - Spy Classes Setup
private extension MediaTypesPresenterTests {
    
    final class MediaTypesViewControllerSpy: BaseViewController, MediaTypesDisplayLogic {
        var mockModel: [MediaTypesViewModel]!
        
        func displayViewModel(_ viewModel: MediaTypesModel.ViewModel) {
            switch viewModel {
                case .mediaTypes(viewModel: let viewModel):
                mockModel = viewModel
            }
        }
        
    }
}
