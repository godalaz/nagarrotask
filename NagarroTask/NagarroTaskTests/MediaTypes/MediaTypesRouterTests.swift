//
//  MediaTypesRouterTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class MediaTypesRouterTests: XCTestCase {
  
  private var router: MediaTypesRouter!
  private var viewController: MediaTypesViewControllerSpy!

  override func setUp() {
    viewController = MediaTypesViewControllerSpy()
    router = MediaTypesRouter(viewController: viewController)
  }

  override func tearDown() {
    viewController = nil
    router = nil
  }
}


// MARK: - Tests
extension MediaTypesRouterTests {

  func textExample() {
  }
    
}


// MARK: - Spy Classes Setup
private extension MediaTypesRouterTests {

  final class MediaTypesViewControllerSpy: UIViewController {
    var dismissExpectation: XCTestExpectation!
    var isDismissed: Bool = false

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
      isDismissed = true
      dismissExpectation.fulfill()
    }
  }
}
