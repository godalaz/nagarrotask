//
//  MediaTypesViewControllerTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class MediaTypesViewControllerTests: XCTestCase {
  
  private var interactor: MediaTypesInteractorSpy!
  private var viewController: MediaTypesViewController!
  private var router: MediaTypesRouterSpy!
  
  override func setUp() {
    viewController = MediaTypesConfig.createModule(factory: self, dataSource: MediaTypesModel.DataSource())
  }
  
  override func tearDown() {
    interactor = nil
    viewController = nil
    router = nil
  }
}



// MARK: - Tests
extension MediaTypesViewControllerTests {
  
  func testExample() {
  }
}


// MARK: - LoginFactorable, Injector
extension MediaTypesViewControllerTests: MediaTypesFactorable {
  
  func makeInteractor(factory: InteractableFactory,
                      viewController: MediaTypesDisplayLogic?,
                      dataSource: MediaTypesModel.DataSource) -> MediaTypesInteractable {
    interactor = MediaTypesInteractorSpy(dataSource: dataSource)
    return interactor
  }
  
  func makeRouter(viewController: UIViewController?) -> MediaTypesRouting {
    router = MediaTypesRouterSpy()
    return router
  }
}


// MARK: - Spy Classes Setup
private extension MediaTypesViewControllerTests {
  
  final class MediaTypesInteractorSpy: MediaTypesInteractable {
    var dataSource: MediaTypesModel.DataSource
    
    init(dataSource: MediaTypesModel.DataSource) {
      self.dataSource = dataSource
    }
    
    func doRequest(_ request: MediaTypesModel.Request) {}
  }
  
  final class MediaTypesRouterSpy: MediaTypesRouting {
    
    func routeTo(_ route: MediaTypesModel.Route) {}
  }
}
