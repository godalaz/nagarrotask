//
//  SearchInteractorTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchInteractorTests: XCTestCase {
    
    private var presenter: SearchPresenterSpy!
    private var interactor: SearchInteractor!
    
    override func setUp() {
        interactor = SearchInteractor(factory: self, viewController: nil, dataSource: SearchModel.DataSource())
    }
    
    override func tearDown() {
        presenter = nil
        interactor = nil
    }
}


// MARK: - Tests
extension SearchInteractorTests {
    
    func testExample() {
    }
}


// MARK: - SearchFactorable
extension SearchInteractorTests: SearchFactorable {
    
    func makePresenter(viewController: SearchDisplayLogic?) -> SearchPresentationLogic {
        presenter = SearchPresenterSpy()
        return presenter
    }
}


// MARK: - Mock Classes Setup
private extension SearchInteractorTests {
    
    final class SearchPresenterSpy: SearchPresentationLogic {
        
        func presentError(error: SearchModel.SearchError) {}
        
        func presentResponse(_ response: SearchModel.Response) {}
    }
}
