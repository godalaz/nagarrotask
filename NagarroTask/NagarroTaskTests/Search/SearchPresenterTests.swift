//
//  SearchPresenterTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchPresenterTests: XCTestCase {
    
    private var presenter: SearchPresenter!
    private var viewController: SearchViewControllerSpy!
    
    override func setUp() {
        viewController = SearchViewControllerSpy()
        presenter = SearchPresenter(viewController: viewController)
    }
    
    override func tearDown() {
        viewController = nil
        presenter = nil
    }
}


// MARK: - Tests
extension SearchPresenterTests {
    
    func testSearchPresenter() {
        
        XCTAssertNil(viewController.errorTitle)
        let error = "test error"
        presenter.presentError(error: .init(errorTitle: error))
        XCTAssertEqual(viewController.errorTitle, error)
    }
}



// MARK: - Mock Classes Setup
private extension SearchPresenterTests {
    
    final class SearchViewControllerSpy: BaseViewController, SearchDisplayLogic {
        var errorTitle: String!
        
        func displayError(_ error: SearchModel.SearchError) {
            errorTitle = error.errorTitle ?? ""
        }
        
        func displayViewModel(_ viewModel: SearchModel.ViewModel) {}
    }
}
