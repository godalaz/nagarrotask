//
//  SearchRouterTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchRouterTests: XCTestCase {
  
  private var router: SearchRouter!
  private var viewController: SearchViewControllerSpy!

  override func setUp() {
    viewController = SearchViewControllerSpy()
    router = SearchRouter(viewController: viewController)
  }

  override func tearDown() {
    viewController = nil
    router = nil
  }
}


// MARK: - Tests
extension SearchRouterTests {

  func testSearchRouterShouldDismissViewController() {
    
    XCTAssertFalse(viewController.isDismissed)
    viewController.dismissExpectation = expectation(description: "dismissExpectation")
    
    router.routeTo(.dismiss)
    wait(for: [viewController.dismissExpectation], timeout: 0.1)
    
    XCTAssertTrue(viewController.isDismissed)
  }
}


// MARK: - Mock Classes Setup
private extension SearchRouterTests {

  final class SearchViewControllerSpy: UIViewController {
    var dismissExpectation: XCTestExpectation!
    var isDismissed: Bool = false

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
      isDismissed = true
      dismissExpectation.fulfill()
    }
  }
}
