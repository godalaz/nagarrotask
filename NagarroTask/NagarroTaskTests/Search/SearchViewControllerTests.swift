//
//  SearchViewControllerTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/26/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchViewControllerTests: XCTestCase {
  
  private var interactor: SearchInteractorSpy!
  private var viewController: SearchViewController!
  private var router: SearchRouterSpy!
  
  override func setUp() {
    viewController = SearchConfig.createModule(factory: self, dataSource: SearchModel.DataSource())
  }
  
  override func tearDown() {
    interactor = nil
    viewController = nil
    router = nil
  }
}



// MARK: - Tests
extension SearchViewControllerTests {
  
  func testExample() {}
}


// MARK: - LoginFactorable, Injector
extension SearchViewControllerTests: SearchFactorable {
  
  func makeInteractor(factory: InteractableFactory,
                      viewController: SearchDisplayLogic?,
                      dataSource: SearchModel.DataSource) -> SearchInteractable {
    interactor = SearchInteractorSpy(dataSource: dataSource)
    return interactor
  }
  
  func makeRouter(viewController: UIViewController?) -> SearchRouting {
    router = SearchRouterSpy()
    return router
  }
}


// MARK: - Mock Classes Setup
private extension SearchViewControllerTests {
  
  final class SearchInteractorSpy: SearchInteractable {
    var dataSource: SearchModel.DataSource
    
    init(dataSource: SearchModel.DataSource) {
      self.dataSource = dataSource
    }
    
    func doRequest(_ request: SearchModel.Request) {}
  }
  
  final class SearchRouterSpy: SearchRouting {
    
    func routeTo(_ route: SearchModel.Route) {}
  }
}
