//
//  SearchResultInteractorTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchResultInteractorTests: XCTestCase {
    
    private var presenter: SearchResultPresenterSpy!
    private var interactor: SearchResultInteractor!
    private var service: TaskServiceSpy!
    
    override func setUp() {
        interactor = SearchResultInteractor(factory: self, viewController: nil, dataSource: SearchResultModel.DataSource(searchKeyword: "test", selectedMedia: ["testMedia"]))
    }
    
    override func tearDown() {
        presenter = nil
        interactor = nil
    }
}


// MARK: - Tests
extension SearchResultInteractorTests {
    
    func testExample() {
        XCTAssertNil(presenter.mockSearchResult)
        interactor.doRequest(.getSearchResults)
        var mockModel = [SearchResult]()
        
        mockModel.append(.init(wrapperType: nil, kind: nil, artistid: nil, collectionid: nil, trackid: nil, artistName: "test name", collectionName: nil, trackName: "test track name", collectionCensoredName: nil, trackCensoredName: nil, artistViewurl: nil, collectionViewurl: nil, trackViewurl: nil, previewurl: nil, artworkUrl30: nil, artworkUrl60: nil, artworkUrl100: "test image", collectionPrice: nil, trackPrice: nil, releaseDate: nil, collectionExplicitness: nil, trackExplicitness: nil, discCount: nil, discNumber: nil, trackCount: nil, trackNumber: nil, trackTimeMillis: nil, country: nil, currency: nil, primaryGenreName: nil, contentAdvisoryRating: nil))
        XCTAssertEqual(mockModel, service.mockModel)

    }
}


// MARK: - SearchResultFactorable
extension SearchResultInteractorTests: SearchResultFactorable {
    
    func makePresenter(viewController: SearchResultDisplayLogic?) -> SearchResultPresentationLogic {
        presenter = SearchResultPresenterSpy()
        return presenter
    }
    
    func makeTaskService() -> TaskServiceProtocol {
        service = TaskServiceSpy()
        return service
    }
}


// MARK: - Spy Classes Setup
private extension SearchResultInteractorTests {
    
    final class SearchResultPresenterSpy: SearchResultPresentationLogic {
        var mockSearchResult: [SearchResult]!
        
        func presentResponse(_ response: SearchResultModel.Response) {
            switch response {
            case .getSearchResults(response: let response):
                mockSearchResult = response
            }
        }
    }
    
    final class TaskServiceSpy: TaskServiceProtocol {
        var mockModel = [SearchResult]()
        
        func getMediaTypes(completion: @escaping (Result<[MediaTypesResponse], Error>) -> Void) {
        }
        
        func getSearchResults(searchKeyword: String, mediaTypes: String, completion: @escaping (Result<[SearchResult], Error>) -> Void) {
            mockModel.append(.init(wrapperType: nil, kind: nil, artistid: nil, collectionid: nil, trackid: nil, artistName: "test name", collectionName: nil, trackName: "test track name", collectionCensoredName: nil, trackCensoredName: nil, artistViewurl: nil, collectionViewurl: nil, trackViewurl: nil, previewurl: nil, artworkUrl30: nil, artworkUrl60: nil, artworkUrl100: "test image", collectionPrice: nil, trackPrice: nil, releaseDate: nil, collectionExplicitness: nil, trackExplicitness: nil, discCount: nil, discNumber: nil, trackCount: nil, trackNumber: nil, trackTimeMillis: nil, country: nil, currency: nil, primaryGenreName: nil, contentAdvisoryRating: nil))
            completion(.success(mockModel))
        }
    }
}

