//
//  SearchResultPresenterTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchResultPresenterTests: XCTestCase {
    
    private var presenter: SearchResultPresenter!
    private var viewController: SearchResultViewControllerSpy!
    
    override func setUp() {
        viewController = SearchResultViewControllerSpy()
        presenter = SearchResultPresenter(viewController: viewController)
    }
    
    override func tearDown() {
        viewController = nil
        presenter = nil
    }
}


// MARK: - Tests
extension SearchResultPresenterTests {
    
    func testSearchResultPresenter() {
        XCTAssertNil(viewController.mockData)
        var searchResultMock = [SearchResult]()
        let transfomer = SearchResultsTransformer()
        
        searchResultMock.append(.init(wrapperType: nil, kind: nil, artistid: nil, collectionid: nil, trackid: nil, artistName: "test name", collectionName: nil, trackName: "test track name", collectionCensoredName: nil, trackCensoredName: nil, artistViewurl: nil, collectionViewurl: nil, trackViewurl: nil, previewurl: nil, artworkUrl30: nil, artworkUrl60: nil, artworkUrl100: "test image", collectionPrice: nil, trackPrice: nil, releaseDate: nil, collectionExplicitness: nil, trackExplicitness: nil, discCount: nil, discNumber: nil, trackCount: nil, trackNumber: nil, trackTimeMillis: nil, country: nil, currency: nil, primaryGenreName: nil, contentAdvisoryRating: nil))
        presenter.presentResponse(.getSearchResults(response: searchResultMock))
        XCTAssertEqual(viewController.mockData, transfomer.tranformSearchResultToViewModel(results: searchResultMock))
    }
}



// MARK: - Spy Classes Setup
private extension SearchResultPresenterTests {
    
    final class SearchResultViewControllerSpy: BaseViewController, SearchResultDisplayLogic {
        var mockData: [SearchResultsViewModel]!
        func displayViewModel(_ viewModel: SearchResultModel.ViewModel) {
            switch viewModel {
                
            case .searchResultsViewModel(viewModel: let viewModel):
                mockData = viewModel
            }
        }
    }
}
