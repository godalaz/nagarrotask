//
//  SearchResultRouterTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchResultRouterTests: XCTestCase {
  
  private var router: SearchResultRouter!
  private var viewController: SearchResultViewControllerSpy!

  override func setUp() {
    viewController = SearchResultViewControllerSpy()
    router = SearchResultRouter(viewController: viewController)
  }

  override func tearDown() {
    viewController = nil
    router = nil
  }
}


// MARK: - Tests
extension SearchResultRouterTests {

  func textExample() {
  }
}


// MARK: - Spy Classes Setup
private extension SearchResultRouterTests {

  final class SearchResultViewControllerSpy: UIViewController {
    var dismissExpectation: XCTestExpectation!
    var isDismissed: Bool = false

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
      isDismissed = true
      dismissExpectation.fulfill()
    }
  }
}
