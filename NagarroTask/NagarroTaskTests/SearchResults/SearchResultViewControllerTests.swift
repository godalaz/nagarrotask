//
//  SearchResultViewControllerTests.swift
//  NagarroTask
//
//  Created by Mohamed Goda on 4/29/21.
//  Copyright (c) 2021 Mohamed Goda. All rights reserved.
//


import XCTest
@testable import NagarroTask


final class SearchResultViewControllerTests: XCTestCase {
  
  private var interactor: SearchResultInteractorSpy!
  private var viewController: SearchResultViewController!
  private var router: SearchResultRouterSpy!
  
  override func setUp() {
    viewController = SearchResultConfig.createModule(factory: AppInjector(), dataSource: SearchResultModel.DataSource(searchKeyword: "test", selectedMedia: ["testMedia"]))
  }
  
  override func tearDown() {
    interactor = nil
    viewController = nil
    router = nil
  }
}



// MARK: - Tests
extension SearchResultViewControllerTests {
  
  func testExample() {
  }
}


// MARK: - LoginFactorable, Injector
extension SearchResultViewControllerTests: SearchResultFactorable {
  
  func makeInteractor(factory: InteractableFactory,
                      viewController: SearchResultDisplayLogic?,
                      dataSource: SearchResultModel.DataSource) -> SearchResultInteractable {
    interactor = SearchResultInteractorSpy(dataSource: dataSource)
    return interactor
  }
  
  func makeRouter(viewController: UIViewController?) -> SearchResultRouting {
    router = SearchResultRouterSpy()
    return router
  }
}


// MARK: - Spy Classes Setup
private extension SearchResultViewControllerTests {
  
  final class SearchResultInteractorSpy: SearchResultInteractable {
    var dataSource: SearchResultModel.DataSource
    
    init(dataSource: SearchResultModel.DataSource) {
      self.dataSource = dataSource
    }
    
    func doRequest(_ request: SearchResultModel.Request) {
      
      #warning("handler requests")
    }
  }
  
  final class SearchResultRouterSpy: SearchResultRouting {
    
    func routeTo(_ route: SearchResultModel.Route) {
      
      #warning("handle routes")
    }
  }
}
